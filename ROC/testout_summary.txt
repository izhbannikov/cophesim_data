Input type: plink
Output type: plink
Input data: /Users/ilya/Projects/cophesim_data/ROC/sim.plink
Cefile: /Users/ilya/Projects/cophesim_data/ROC/effects.txt
Epifile: None
Output prefix: /Users/ilya/Projects/cophesim_data/ROC/testout
alpha: 0.2138
Number of SNPs: 1000
Number of individuals: 10000
Number of cases: 4076
Number of controls: 5924
Average for continous phenotype: -1.63296276027
